package pl.sdacademy.xml.main;

/**
 * Created by patry on 18.02.2017.
 */
public class User {

    private String name;
    private String surname;
    private String address;
    private String telNumber;
    private String creditCardNumber;
    private String pesel;

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getAddress() {
        return address;
    }

    public String getTelNumber() {
        return telNumber;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public String getPesel() {
        return pesel;
    }
}
