package pl.sdacademy.xml.main;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by patry on 16.02.2017.
 */
public class XMLExcercise {


    private final String path = "src\\pl\\sdacademy\\xml\\resources\\";


    public void readXMLFile(String filename) throws Exception {
        File f = new File(path + filename); //tworzymy uchwyt do pliku z którego chcemy czytać
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();  //zwraca dokument który jest instancją

        DocumentBuilder db = dbf.newDocumentBuilder();

        Document doc = db.parse(f);
        doc.getDocumentElement().normalize(); //dokonujemy normalizacji dokumentu żeby wszystko miało jednolitą struktur
        NodeList nList = doc.getElementsByTagName("staff");
        for (int i = 0; i < nList.getLength(); ++i) {
            Node n = nList.item(i);

            if (n.getNodeType() == Node.ELEMENT_NODE) { //spr czy to co wybralem to jest na pewno elementem node (enumem)
                Element elem = (Element) n; //przerzutowanie daje możliwość wyświetlenia

                System.out.println("ID: " + elem.getAttribute("id"));
                System.out.println("Imie i nazwisko: " +
                        elem.getElementsByTagName("firstname").item(0).getTextContent() +
                        " " + elem.getElementsByTagName("lastname").item(0).getTextContent());
            }
        }


    }

    public void writeXMLFile(String filename) throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.newDocument();

        Element root = doc.createElement("root");

        Element person = doc.createElement("person");
        person.setAttribute("id", "5");

        Element name = doc.createElement("name");
        name.appendChild(doc.createTextNode("Patryk"));

        Element lastname = doc.createElement("lastname");
        lastname.appendChild(doc.createTextNode("Testowy"));

        person.appendChild(name);
        person.appendChild(lastname);

        root.appendChild(person);
        doc.appendChild(root);

        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer t = tf.newTransformer(); //przygotowujemy transformera ktory z obiekt xml przerobi nam na stringa sml

        DOMSource source = new DOMSource(doc);//modelowy widok dokumentu (document object modeling).
        StreamResult rs = new StreamResult(new File(path + filename));
        t.transform(source, rs); //przetransformuj z source na streamresult;

    }


    public class EmployeesSalary {

        public void getAvgSalary(String filename) throws Exception {

            File f = new File(path + filename);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();

            Document doc = db.parse(f);
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("staff");
            List<Double> salariesList = new LinkedList<>();
            for (int i = 0; i < nList.getLength(); ++i) {
                Node n = nList.item(i);


                if (n.getNodeType() == Node.ELEMENT_NODE) {
                    Element elem = (Element) n;
//                    List<Double> salariesList = new LinkedList<>();
                    double salary = Double.parseDouble(elem.getElementsByTagName("salary").item(0).getTextContent());
                    salariesList.add(salary);


                }
/*
                List<Person> filtered =
                        persons
                                .stream()

                }
                salariesList.*/
            }


        }

        public String getPath() {
            return path;
        }
    }
}


