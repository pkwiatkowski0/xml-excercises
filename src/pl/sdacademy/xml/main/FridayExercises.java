package pl.sdacademy.xml.main;
// 1.Dane jest osiedle bloków.Każdy blok zawiera 2klatki,po 2piętra(+parter),na każdym piętrze znajdują się dwa
// mieszkania.Napisz skrypt,który wyświetli adres każdego mieszkania tak,aby można było zaadresować kopertę.
// Bloki znajdują się tylko pod numerami nieparzystymi pierwszy blok znajduje się pod numerem 1.Funkcja powinna
// przyjmować nazwę ulicy oraz ostatni numer,który występuje na danej ulicy(może być parzysty lub nieparzysty)
// oraz na wyjściu drukować poszczególne adresy.

// ul. Algorytmiczna 1/A/1
// ul. Algorytmiczna 1/A/2
// ul. Algorytmiczna 1/A/6
// ul. Algorytmiczna 1/B/7
//...
// ul. Algorytmiczna 1/B/12
// ul. Algorytmiczna 3/A/1


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by patry on 17.02.2017.
 */
public class FridayExercises {


    public void adresses(String streetName, int number) {

        //aby zminimalizowac zlozonosc obliczeniowa
        //iterujemy się przez bloki. Należy zastanowic sie nad poprawna konstrukcja petli for
        for (int i = 1; i <= number; i += 2) {
            for (int j = 1; j <= 12; j++) {
                String cageNumber = "A";
                if (j > 6) {
                    cageNumber = "B";
                    System.out.println("ul. " + streetName + " " + i + "/" + cageNumber + "/" + j);
                } else {
                    System.out.println("ul. " + streetName + " " + i + "/" + cageNumber + "/" + j);
                }

            }
        }
        //iterujemy sie przez numery mieszkań (czyli od 1 do 12)
        //sprawdzamy czy przekroczył numer mieszkania (większy od 6)
        //jeśli tak, to zmieniamy numer klatki
        //drukujemy informacje o wybranym adresie


// Napisz funkcję, która zwróci do konsoli kody pocztowe. Funkcja przyjmie dwa argumenty (stringi!),
// w postaci np.: getPostCodes("14-999", "15-500"). Funkcja ma za zadanie zwrócić do konsoli, wszystkie kody
// z przedziału podanego w argumentach tj. 14-999 15-000 15-001 ... 15-500

    }

    public void getPostCodes(String postCode1, String postCode2) {

        String[] parts1 = postCode1.split("-");
        String[] parts2 = postCode2.split("-");
        String part1 = parts1[0] + parts1[1];
        String part2 = parts2[0] + parts2[1];
        int intPostCode1 = Integer.parseInt(part1);
        int intPostCode2 = Integer.parseInt(part2);

        System.out.println("First PostCode: " + intPostCode1);
        System.out.println("Last PostCode: " + intPostCode2);

        int counter = 1;
        for (int i = intPostCode1; i <= intPostCode2; i++) {
            String currentPostCode = "" + i;
            System.out.println(counter + ". \t" + currentPostCode.substring(0, 2) + "-" + currentPostCode.substring(2));
            counter++;
        }
    }


    /*
        Napisz funkcję, która przyjmie trzy argumenty. Pierwszy to String na którym pracujemy,
         drugi to znak (String), który chcemy dodać z lewej strony, trzeci to długość, którą chcemy
         osiągnąć. Przykład: public String lpad(String data, String add, int num); Przykład wywołania:
         lpad("xxx", "=", 10); // =======xxx
    */
    public String lpad(String data, String add, int num) throws MyOwnException {

        if (data.length() < num) {
            int range = num - data.length();
            for (int i = 0; i < range; i++) {
                data = add + "" + data;
            }
        } else {
            throw new MyOwnException("num cannot be less than data.length!");
        }

        return data;
    }



/*
    Napisz funkcję, która wypisze oraz zliczy ilość takich godzin z przedziału, które zawierają tylko
     nie więcej niż 2 różne cyfry. Funkcja przyjmuje przedział czasowy w postaci dwóch stringów np.
     function getTimeByNumbers("21:59:34", "22:00:00"); Zakładamy, że argument pierwszy zawsze jest
     wcześniejszym czasem niż argument drugi, oraz oba argumenty posiadają czas z jednego dnia
     (brak przeskoczeń półncy np. 23:55:00 i 02:03:00 - taki czas nie będzie poprawnym argumentem) oraz,
      że czas zawsze jest w formacie HH:mm:ss. Czas, który powinien być uwzględniany to np. 22:22:22
      (zawiera tylko jedną unikalną cyfrę - 2) 22:22:23 (zawiera dwie unikalne cyfry, a więc spełnia warunek)
       ale np. 21:59:34 już nie będzie policzony, gdyż zawiera 6 różnych cyfr w swojej godzinie. 22:22:34
       (zawiera 3 unikalne cyfry)
*/

/*
    public int getTimeByNumbers(String start, String stop) {
        //1. Pozbywamy sie : z naszych stringow
        //2. dokonujemy iteracji pomiedzy pierwszym a drugim ciagiem znakow
        //  a. Uwaga! sprawdzamy w jaki sposob iteracja przeskakuje pomiedzy sekundami i minutami
        //  b. sprawdzamy czy ilosc cyfr w aktuanie iterowanej godzinie nie przekracza 2
        //      -jezeli nie, to inkrementujemy zmienna z iloscia naszych godzin
        //      /ten punkt mozna zrealizowac jako osobna f-cje/
        //3. Zwracamy ilosc policzonych godzin



        String[] parts1 = start.split(":");
        String[] parts2 = stop.split(":");
        String part1 = parts1[0] + parts1[1] + parts1[2];
        String part2 = parts2[0] + parts2[1] + parts2[2];
        int intStart = Integer.parseInt(part1);
        int intStop = Integer.parseInt(part2);

        System.out.println("start: " + intStart + " stop: " + intStop);


        Set<String> set1 = new HashSet<>(Arrays.asList(start.split(":")));
        Set<String> set2 = new HashSet<>(Arrays.asList(stop.split(":")));

        System.out.println("Set1: " + set1);
        System.out.println("Set2: " + set2);
*/
/*
        for (int i = intStart; i <= intStop; i++) {
            if (i % 60 == 0) {
                i += 40;
            }
        }
        return 0;
    }*/


        //    metoda sprawdzajaca ile jest unikalnych cyfr w ciagu
        public int getUniqueNumbers (String number) throws MyOwnException {
            Set<String> s = new HashSet<>();
            if (number.length() < 6) {
                s.add("0");
            }

            for (int i = 0; i < number.length(); i++) {

                String lp = lpad(i + "", "0", 6);
                s.add(number.charAt(i) + "");
            }
            return s.size();

        }
    }
